//
//  RoomsTableViewCell.swift
//  rentalz_mobile
//
//  Created by Phuong Nguyen on 25/09/2021.
//

import UIKit
protocol RoomCellProtocol {
    func btnClick(index: Int, isFirst: Bool)
    func updateRoomValue(index: Int, val: String, isAmount: Bool)
}

class RoomsTableViewCell: UITableViewCell {
    var delegate: RoomCellProtocol?
    var index: Int = 0
    @IBOutlet weak var roomNameTf: UITextField!
    
    @IBOutlet weak var amountTf: UITextField!
    @IBOutlet weak var btnView: UIButton!
    var isFirstCell = false
    
    @IBAction func editingChange(_ sender: UITextField) {
        if let text = sender.text {
            delegate?.updateRoomValue(index: index, val: text, isAmount: false)
        }
    }
    
    @IBAction func amountChange(_ sender: UITextField) {
        if let text = sender.text {
            delegate?.updateRoomValue(index: index, val: text, isAmount: true)
        }
    }
    @IBAction func btnClick(_ sender: UIButton) {
        delegate?.btnClick(index: index, isFirst: isFirstCell)
    }
    func refresh() {
        roomNameTf.layer.borderColor = UIColor.black.cgColor
        amountTf.layer.borderColor = UIColor.black.cgColor
        roomNameTf.text = ""
        amountTf.text = ""
    }
    func displayError(isVal: Bool) {
        if isVal == false {
            roomNameTf.layer.borderColor = UIColor.red.cgColor
            amountTf.layer.borderColor = UIColor.red.cgColor
        } else {
            roomNameTf.layer.borderColor = UIColor.black.cgColor
            amountTf.layer.borderColor = UIColor.black.cgColor
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        roomNameTf.layer.borderWidth = 0.3
        amountTf.layer.borderWidth = 0.3
        if isFirstCell == true {
            btnView.setImage(UIImage(systemName: "plus"), for: .normal)
        } else {
            btnView.setImage(UIImage(systemName: "minus"), for: .normal)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
