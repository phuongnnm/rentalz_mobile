//
//  CheckBoxImageView.swift
//  rentalz_mobile
//
//  Created by Phuong Nguyen on 26/09/2021.
//

import Foundation
import UIKit

class CheckBoxImgView: UIImageView {
    
    var isChecked = false {
        didSet {
            if isChecked == true {
                let config = UIImage.SymbolConfiguration(scale: .large)
                self.image = UIImage(systemName: "checkmark", withConfiguration: config)
            } else {
                self.image = nil
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.systemPurple.cgColor
        self.tintColor = .purple
        self.contentMode = .center
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}
