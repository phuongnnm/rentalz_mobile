//
//  CreatPropViewController.swift
//  rentalz_mobile
//
//  Created by Phuong Nguyen on 25/09/2021.
//

import UIKit

class Room {
    var name: String?
    var amount: Int?
    init (name: String?, amount: Int?) {
        self.name = name
        self.amount = amount
    }
}

class CreatPropViewController: BaseViewController, RoomCellProtocol, UITableViewDelegate, UITableViewDataSource  {
    func updateRoomValue(index: Int, val: String, isAmount: Bool) {
        if isAmount == false {
            rooms[index].name = val
        } else {
            rooms[index].amount = Int(val)
        }
    }
    
    @IBOutlet weak var priceVal: UILabel!
    @IBOutlet weak var nameVal: UILabel!
    let validator = Validator()
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    func btnClick(index: Int, isFirst: Bool) {
        if isFirst == false {
            print(index)
            let cell = self.RoomsTableView.cellForRow(at: [0, index]) as! RoomsTableViewCell
            cell.refresh()
            rooms.remove(at: index)
            
            
        } else {
            let room = Room(name: "", amount: nil)
            rooms.append(room)
        }
    }
    
    func validRooms() -> Bool {
        var a: [Bool] = []
        for (i, _) in self.rooms.enumerated() {
            let cell = self.RoomsTableView.cellForRow(at: [0, i]) as! RoomsTableViewCell
            if validator.validate(tf: cell.amountTf, controller: self, with: [.notEmpty]) == nil {
                a.append(true)
            } else {
                a.append(false)
            }
            
            if validator.validate(tf: cell.roomNameTf, controller: self, with: [.notEmpty]) == nil {
                a.append(true)
            } else {
                a.append(false)
            }
        }
        if a.contains(false) {return false}
        return true
    }
    
    @IBOutlet weak var noteTv: UITextView!
    @IBAction func submitClick(_ sender: UIButton) {
        let a = validator.validate(tf: priceTf, controller: self, with: [.notEmpty])
        let b = validator.validate(tf: nameTf, controller: self, with: [.notEmpty])
        let c = validRooms()
        
        if a == nil && b == nil && c == true {
            showAlert(title: "Success", message: "Your data has been submitted")
           
        }
    }
    
    @IBOutlet weak var priceTf: UITextField!
    @IBOutlet weak var nameTf: UITextField!
    @IBOutlet weak var statusCollectionView: UICollectionView!
    @IBOutlet weak var RoomsTableView: UITableView!
    @IBOutlet weak var proptypeCollectionView: UICollectionView!
    var rooms: [Room] = [Room(name: "", amount: nil)] {
        didSet {
            RoomsTableView.reloadData()
            DispatchQueue.main.async {
                self.RoomsTableView.scrollToRow(at: IndexPath(item: self.rooms.count - 1, section: 0), at: .bottom, animated: true)
            }
        }
    }
    let statuses = ["Furnished", "Part Furnished", "Unfurnished"]
    
    var roomTypes = ["Flat", "House", "Bungalow", "Mansion"]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        configCollectionView()
        configTableView()
        priceTf.layer.borderWidth = 0.5
        priceTf.layer.cornerRadius = 12.0
        nameTf.layer.cornerRadius = 12.0
        nameTf.layer.borderWidth = 0.5
        noteTv.layer.borderWidth = 0.4
        noteTv.layer.cornerRadius = 12.0
        noteTv.clipsToBounds = true
        nameTf.clipsToBounds = true
        priceTf.clipsToBounds = true
    }
   
    func configCollectionView() {
        proptypeCollectionView.delegate = self
        proptypeCollectionView.dataSource = self
        statusCollectionView.delegate = self
        statusCollectionView.dataSource = self
        statusCollectionView.register(UINib(nibName: "PropTypeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PropTypeCollectionViewCell")
        
        proptypeCollectionView.register(UINib(nibName: "PropTypeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PropTypeCollectionViewCell")
        let width = view.frame.size.width / 2 - 10
        let layout = proptypeCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        statusCollectionView.translatesAutoresizingMaskIntoConstraints = false
        proptypeCollectionView.translatesAutoresizingMaskIntoConstraints = false
        layout.itemSize = CGSize(width: width, height: 50)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        let statusLayout = statusCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        statusLayout.itemSize = CGSize(width: width, height: 50)
        statusLayout.minimumLineSpacing = 0
        statusLayout.minimumInteritemSpacing = 0
    }
    
    func configTableView() {
        RoomsTableView.delegate = self
        RoomsTableView.dataSource = self
        RoomsTableView.register(UINib(nibName: "RoomsTableViewCell", bundle: nil), forCellReuseIdentifier: "RoomsTableViewCell")
    }
}

extension CreatPropViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == statusCollectionView {
            return statuses.count
        }
        return roomTypes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PropTypeCollectionViewCell", for: indexPath) as? PropTypeCollectionViewCell else { return UICollectionViewCell()}
        if collectionView == proptypeCollectionView {
            cell.placeLabel.text = roomTypes[indexPath.row]
        } else {
            cell.placeLabel.text = statuses[indexPath.row]
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! PropTypeCollectionViewCell
        let config = UIImage.SymbolConfiguration(scale: .medium)
        cell.checkBox.image = UIImage(systemName: "checkmark", withConfiguration: config)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! PropTypeCollectionViewCell
        cell.checkBox.image = nil
    }
}

extension CreatPropViewController {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rooms.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RoomsTableViewCell") as! RoomsTableViewCell
        cell.delegate = self
        cell.index = indexPath.row
        if indexPath.row == 0 {
            cell.isFirstCell = true
            cell.btnView.setImage(UIImage(systemName: "plus"), for: .normal)
        } else {
            cell.isFirstCell = false
            cell.btnView.setImage(UIImage(systemName: "minus"), for: .normal)
        }
        return cell
    }
}

class Validator {
    func validate(text: String, with rules: [Rule]) -> String? {
        return rules.compactMap({ $0.check(text) }).first
    }
    
    func validate(tf: UITextField, controller: CreatPropViewController, with rules: [Rule]) -> String? {
        guard let message = validate(text: tf.text ?? "", with: rules) else {
            tf.layer.borderColor = UIColor.black.cgColor
            if tf == controller.nameTf {
                controller.nameVal.text = ""
            }
            if tf == controller.priceTf {
                controller.priceVal.text = ""
            }
            return nil
        }
        
        tf.layer.borderColor = UIColor.red.cgColor
        if tf == controller.nameTf {
            controller.nameVal.text = message
        }
        if tf == controller.priceTf {
            controller.priceVal.text = message
        }
        
        for (i, room) in controller.rooms.enumerated() {
            let cell = controller.RoomsTableView.cellForRow(at: [0, i]) as! RoomsTableViewCell
            
            if tf == cell.amountTf {
                guard let a = validate(text: "" , with: rules) else {
                    cell.displayError(isVal: true)
                    return nil
                }
                
                cell.displayError(isVal: false)
                return a
                
            }
            if tf == cell.roomNameTf {
                guard let a = validate(text: room.name ?? "", with: rules) else {
                    cell.displayError(isVal: true)
                    return nil
                }

                cell.displayError(isVal: false)
                return a
            }
        }
        return message
    }
}

struct Rule {
    // Return nil if matches, error message otherwise
    let check: (String) -> String?
    
    static let notEmpty = Rule(check: {
        return $0.isEmpty ? "Please enter some text." : nil
    })
}
