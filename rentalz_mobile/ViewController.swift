//
//  ViewController.swift
//  rentalz_mobile
//
//  Created by Phuong Nguyen on 25/09/2021.
//

import UIKit

class ViewController: BaseViewController {

    @IBOutlet weak var purpleView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        purpleView.layer.cornerRadius = 25.0
        view.backgroundColor = purpleView.backgroundColor
        purpleView.layer.maskedCorners = [.layerMinXMaxYCorner]
        purpleView.clipsToBounds = true
    }


}

